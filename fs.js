const { executionAsyncResource } = require('async_hooks')
const fs = require('fs')
const path=require('path')

fs.mkdir(path.join(__dirname,'notes'), err=>{
    if(err) throw err

    console.log('Папка створена')
})

fs.writeFile(
    path.join(__dirname,'notes', 'mynotes.txt'),
    'Hello word',
    (err)=>{
        if(err) throw err
        console.log('Файл створено')

        fs.appendFile(
            path.join(__dirname,'notes','mynotes.txt'),
            ' From append file',
            err =>{
                if (err) throw err
                console.log('Файл змінено')

                fs.readFile(
                    path.join(__dirname,'notes','mynotes.txt'),
                    'utf-8',
                    (err, data)=>{
                        if (err) throw err
                        console.log(data)
                    }
                )
            }
        )
    }
)
fs.rename(
    path.join(__dirname,'notes', 'mynotes.txt'),
    path.join(__dirname, 'notes', 'notes.txt'),
    err=>{
        if (err) throw err
        console.log('Файл перейменовано')
    }
)