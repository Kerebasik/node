const path =require('path')

console.log(__filename) // Весь путь к файлу

console.log(path.basename(__filename)) //Возвращает конечную часть пути
console.log(path.dirname(__filename))  //Возвращает имя каталога пути
console.log(path.extname(__filename))  //Возарвщвет расширение файла
console.log(path.parse(__filename))    //Возвращает объект, свойства которого представляют важные элементы пути.
console.log(path.join(__dirname, 'test', 'second.html')) //Объединяет все заданные сегменты пути вместе с использованием разделителя, зависящего от платформы, в качестве разделителя, а затем нормализует полученный путь.
console.log(path.resolve(__dirname, 'test', 'second.html')) //Преобразует последовательность путей или сегментов пути в абсолютный путь.
console.log(path.isAbsolute(__filename)) //Определяет, является ли путь абсолютным путем.
console.log(path.win32) //Свойство path.win32 обеспечивает доступ к специфичным для Windows реализациям методов пути.